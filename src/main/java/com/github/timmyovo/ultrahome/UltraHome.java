package com.github.timmyovo.ultrahome;

import com.github.skystardust.ultracore.bukkit.commands.MainCommandSpec;
import com.github.skystardust.ultracore.bukkit.modules.inventory.InventoryBuilder;
import com.github.skystardust.ultracore.bukkit.modules.item.ItemFactory;
import com.github.skystardust.ultracore.core.PluginInstance;
import com.github.skystardust.ultracore.core.configuration.ConfigurationManager;
import com.github.skystardust.ultracore.core.database.newgen.DatabaseManager;
import com.github.skystardust.ultracore.core.exceptions.ConfigurationException;
import com.github.skystardust.ultracore.core.exceptions.DatabaseInitException;
import com.github.timmyovo.ultrahome.config.GeneralConfiguration;
import com.github.timmyovo.ultrahome.database.HomeEntry;
import com.github.timmyovo.ultrahome.database.HomeModel;
import com.github.timmyovo.ultrahome.database.HomeRequestEntry;
import com.google.common.collect.Maps;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Getter
public final class UltraHome extends JavaPlugin implements PluginInstance {
    private static UltraHome ultraHome;
    private DatabaseManager databaseManager;
    private ConfigurationManager configurationManager;
    private GeneralConfiguration generalConfiguration;

    public static UltraHome getUltraHome() {
        return ultraHome;
    }

    public static void sendToServer(Player player, String serverName) {
        ByteArrayDataOutput byteArrayDataOutput = ByteStreams.newDataOutput();
        byteArrayDataOutput.writeUTF("Connect");
        byteArrayDataOutput.writeUTF(serverName);
        player.sendPluginMessage(ultraHome, "BungeeCord", byteArrayDataOutput.toByteArray());
    }

    @Override
    public void onEnable() {
        UltraHome.ultraHome = this;
        try {
            this.databaseManager = DatabaseManager.newBuilder()
                    .withName(getName())
                    .withOwnerPlugin(this)
                    .withModelClass(Arrays.asList(HomeEntry.class, HomeRequestEntry.class, HomeModel.class))
                    .withSqlConfiguration(DatabaseManager.setupDatabase(this))
                    .build()
                    .openConnection();
        } catch (DatabaseInitException | ConfigurationException e) {
            getLogger().warning("数据库初始化失败!");
            getLogger().warning(e.getLocalizedMessage());
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }
        configurationManager = new ConfigurationManager(this);
        configurationManager.
                registerConfiguration("generalConfiguration",()-> GeneralConfiguration.builder()
                        .serverName("exampleServerName")
                        .maxHomeLimited("你无法设置更多家了!")
                        .homeLocationUpdated("家的位置已更新")
                        .homeSaved("储存家成功!")
                        .homeGuiName("回家系统控制面板")
                        .removeHomeMessage("删除成功!")
                        .setHomeButtonName("点击设置家")
                        .setHomeButtonLore(Collections.singletonList("您也可以使用指令设置"))
                        .guiItemCustomLore(Collections.singletonList("右键点击删除!"))
                        .build())
                .init(getClass(), this)
                .start();
        for (Field declaredField : generalConfiguration.getClass().getDeclaredFields()) {
            try {
                if (declaredField.getType()==String.class) {
                    declaredField.setAccessible(true);
                    String s = (String) declaredField.get(generalConfiguration);
                    if (s!=null) {
                        declaredField.set(generalConfiguration,s.replace("&","§"));
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> HomeRequestEntry.db()
                .find(HomeRequestEntry.class)
                .findList()
                .stream()
                .filter(homeRequestEntry -> {
                    Optional<HomeEntry> homeByUID = HomeManager.getHomeByUID(homeRequestEntry.getHomeUid());
                    return homeByUID.isPresent() && homeByUID.get().getServerName().equals(generalConfiguration.getServerName());
                })
                .filter(homeRequestEntry -> Bukkit.getPlayer(homeRequestEntry.getPlayer()) != null)
                .forEach(homeRequestEntry -> {
                    HomeEntry homeEntry = HomeManager.getHomeByUID(homeRequestEntry.getHomeUid()).get();
                    Player player = Bukkit.getPlayer(homeRequestEntry.getPlayer());
                    player.teleport(homeEntry.toLocation());
                    homeRequestEntry.delete();
                }), 0L, 20L);

        MainCommandSpec.newBuilder()
                .addAlias("sethome")
                .addAlias("ultrasethome")
                .addAlias("ush")
                .withPermission("home.use")
                .withCommandSpecExecutor((commandSender, strings) -> {
                    if (!(commandSender instanceof Player)) {
                        return true;
                    }
                    if (strings.length < 1) {
                        commandSender.sendMessage("/sethome [家的名字]");
                        return true;
                    }
                    Player player = (Player) commandSender;
                    HomeManager.addHome(player,strings[0],player.getLocation());
                    return true;
                })
                .build()
                .register();
        InventoryBuilder builder = InventoryBuilder.builder()
                .displayName(generalConfiguration.getHomeGuiName())
                .size(36)
                .itemMap(Maps.newHashMap())
                .updateMenu(param -> {
                    List<HomeEntry> playerHomeList = HomeManager.getPlayerHomeList(param);
                    Map<Integer, ItemStack> collect = playerHomeList
                            .stream()
                            .collect(Collectors.toMap(playerHomeList::indexOf, (value) -> {
                                ItemFactory itemFactory = new ItemFactory(() -> new ItemStack(Material.WOOD_DOOR))
                                        .setDisplayName(ChatColor.GOLD + value.getName());
                                generalConfiguration.getGuiItemCustomLore()
                                        .stream()
                                        .map(str->str.replace("$target$",value.getServerName()))
                                        .forEach(itemFactory::addLore);
                                net.minecraft.server.v1_8_R3.ItemStack asNMSCopy = CraftItemStack.asNMSCopy(itemFactory.pack());
                                NBTTagCompound tag = asNMSCopy.getTag();
                                tag.setString("server",value.getServerName());
                                tag.setString("uid",value.getHomeUid().toString());
                                return CraftItemStack.asBukkitCopy(asNMSCopy);
                            }));
                    ItemFactory itemFactory = new ItemFactory(() -> new ItemStack(Material.REDSTONE_BLOCK))
                            .setDisplayName(generalConfiguration.getSetHomeButtonName());
                    generalConfiguration.getSetHomeButtonLore().forEach(itemFactory::addLore);
                    collect.put(31,itemFactory.pack());
                    return (HashMap<Integer, ItemStack>) collect;
                })
                .build()
                .onClickListenerAdv(inventoryClickEvent -> {
                    ItemStack itemStack = inventoryClickEvent.getCurrentItem();
                    Player player = (Player) inventoryClickEvent.getWhoClicked();
                    if (itemStack == null) {
                        return;
                    }
                    if (itemStack.getItemMeta() == null) {
                        return;
                    }
                    if (itemStack.getItemMeta().hasDisplayName()) {
                        if (itemStack.getItemMeta().getDisplayName().equals(generalConfiguration.getSetHomeButtonName())) {
                            StringBuilder name = new StringBuilder();
                            Calendar instance = Calendar.getInstance();
                            name.append(instance.get(Calendar.YEAR));
                            name.append(instance.get(Calendar.MONTH));
                            name.append(instance.get(Calendar.DAY_OF_MONTH));
                            name.append(instance.get(Calendar.HOUR_OF_DAY));
                            name.append(instance.get(Calendar.MINUTE));
                            name.append(instance.get(Calendar.SECOND));
                            HomeManager.addHome(player,name.toString(),player.getLocation());
                            player.closeInventory();
                            player.chat("/ultrahome");
                            return;
                        }
                    }
                    net.minecraft.server.v1_8_R3.ItemStack asNMSCopy = CraftItemStack.asNMSCopy(itemStack);
                    if (!asNMSCopy.getTag().hasKey("uid")) {
                        return;
                    }
                    UUID homeUid = UUID.fromString(asNMSCopy.getTag().getString("uid"));
                    HomeManager.getHomeByUID(homeUid).ifPresent(homeEntry -> {
                        if (inventoryClickEvent.getClick().isLeftClick()) {
                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b新龙腾 &7>>> &a已将您传送至你设置的家：&6" + homeEntry.getName()));
                            if (!homeEntry.getServerName().equals(generalConfiguration.getServerName())) {
                                sendToServer(player, homeEntry.getServerName());
                            }
                            HomeRequestEntry homeRequestEntry = new HomeRequestEntry(player.getUniqueId(), homeEntry.getHomeUid());
                            try {
                                homeRequestEntry.save();
                            } catch (Exception e) {
                                homeRequestEntry.update();
                            }
                        }else if (inventoryClickEvent.getClick().isRightClick()){
                            homeEntry.delete();
                            player.closeInventory();
                            player.chat("/ultrahome");
                            player.sendMessage(generalConfiguration.getRemoveHomeMessage());
                        } else {
                            //un impl
                        }
                    });
                })
                .lock();
        MainCommandSpec.newBuilder()
                .addAlias("home")
                .addAlias("ultrahome")
                .addAlias("uhome")
                .addAlias("uh")
                .withPermission("home.use")
                .withCommandSpecExecutor((commandSender, strings) -> {
                    if (!(commandSender instanceof Player)) {
                        return true;
                    }
                    Player player = (Player) commandSender;
                    if (strings.length < 1) {
                        builder.show(((Player) commandSender));
                        return true;
                    }
                    List<HomeEntry> playerHomeList = HomeManager.getPlayerHomeList(player);
                    if (playerHomeList.isEmpty()) {
                        commandSender.sendMessage("您目前没有设置任何家!");
                        return true;
                    }
                    if (playerHomeList
                            .stream()
                            .noneMatch(homeEntry -> homeEntry.getName().equals(strings[0]))) {
                        player.sendMessage(org.bukkit.ChatColor.GREEN+"====================================");
                        player.sendMessage(org.bukkit.ChatColor.AQUA +"您拥有的家的列表:");
                        playerHomeList.forEach(homeEntry -> {
                            TextComponent textComponent = new TextComponent((playerHomeList.indexOf(homeEntry)+1)+". "+homeEntry.getName());
                            textComponent.setBold(Boolean.TRUE);
                            textComponent.setColor(ChatColor.GOLD);
                            TextComponent hover1 = new TextComponent("点击传送至家: " + homeEntry.getName()+"\n");
                            hover1.setColor(ChatColor.RED);
                            hover1.setBold(Boolean.TRUE);
                            TextComponent hover2 = new TextComponent("所在服务器: " + homeEntry.getServerName());
                            hover2.setColor(ChatColor.RED);
                            hover2.setBold(Boolean.TRUE);
                            textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,new BaseComponent[]{hover1,hover2}));
                            textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/home " + homeEntry.getName()));
                            player.spigot().sendMessage(textComponent);
                        });
                        player.sendMessage(org.bukkit.ChatColor.GREEN+"====================================");
                        return true;
                    }
                    HomeEntry homeEntry = playerHomeList.stream().filter(homeEntry1 -> homeEntry1.getName().equals(strings[0])).findFirst().get();
                    if (!homeEntry.getServerName().equals(generalConfiguration.getServerName())) {
                        sendToServer(player, homeEntry.getServerName());
                    }
                    new HomeRequestEntry(player.getUniqueId(), homeEntry.getHomeUid()).save();
                    return true;
                })
                .build()
                .register();

    }

    public Logger getPluginLogger() {
        return getLogger();
    }
}
