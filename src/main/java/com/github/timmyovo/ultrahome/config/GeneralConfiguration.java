package com.github.timmyovo.ultrahome.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class GeneralConfiguration {
    private String serverName;
    private String maxHomeLimited;
    private String homeLocationUpdated;
    private String homeSaved;
    private String homeGuiName;
    private String removeHomeMessage;
    private String setHomeButtonName;
    private List<String> setHomeButtonLore;
    private List<String> guiItemCustomLore;
}
