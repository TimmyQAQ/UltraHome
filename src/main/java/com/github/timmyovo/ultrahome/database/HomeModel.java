package com.github.timmyovo.ultrahome.database;

import com.github.timmyovo.ultrahome.UltraHome;
import io.ebean.EbeanServer;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class HomeModel {
    public static EbeanServer db() {
        return UltraHome.getUltraHome().getDatabaseManager().getEbeanServer();
    }

    public void delete() {
        db().delete(this);
    }

    public void save() {
        db().save(this);
    }

    public void update() {
        db().update(this);
    }
}
