package com.github.timmyovo.ultrahome.database;

import com.github.timmyovo.ultrahome.UltraHome;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ultrahome_homedata")
public class HomeEntry extends HomeModel {
    @Id
    private UUID homeUid;
    private UUID owner;
    private String name;
    private String serverName;
    private String worldName;
    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;

    public HomeEntry(String name, Player player) {
        this.homeUid = UUID.randomUUID();
        this.owner = player.getUniqueId();
        this.name = name;
        this.serverName = UltraHome.getUltraHome().getGeneralConfiguration().getServerName();
        this.worldName = player.getWorld().getName();
        this.x = player.getLocation().getX();
        this.y = player.getLocation().getY();
        this.z = player.getLocation().getZ();
        this.yaw = player.getLocation().getYaw();
        this.pitch = player.getLocation().getPitch();
    }

    public Location toLocation() {
        return new Location(Bukkit.getWorld(worldName), x, y, z, yaw, pitch);
    }
}
