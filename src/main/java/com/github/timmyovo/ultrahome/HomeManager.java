package com.github.timmyovo.ultrahome;

import com.github.timmyovo.ultrahome.config.GeneralConfiguration;
import com.github.timmyovo.ultrahome.database.HomeEntry;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachmentInfo;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class HomeManager {
    public static List<HomeEntry> getPlayerHomeList(UUID player) {
        return HomeEntry.db()
                .find(HomeEntry.class)
                .where()
                .eq("owner", player)
                .findList();
    }

    public static List<HomeEntry> getPlayerHomeList(Player player) {
        return getPlayerHomeList(player.getUniqueId());
    }

    public static Optional<HomeEntry> getHomeByUID(UUID uuid) {
        return HomeEntry.db().find(HomeEntry.class)
                .where()
                .eq("homeUid", uuid)
                .findOneOrEmpty();
    }
    public static boolean addHome(Player player,String name, Location location){
        GeneralConfiguration generalConfiguration = UltraHome.getUltraHome().getGeneralConfiguration();
        Integer maxHomeLimit = player.getEffectivePermissions()
                .stream()
                .map(PermissionAttachmentInfo::getPermission)
                .filter(permission -> permission.startsWith("home.limit."))
                .map(permission -> Integer.valueOf(permission.replace("home.limit.", "")))
                .max(Integer::compareTo)
                .orElse(0);
        if ((HomeManager.getPlayerHomeList(player).size() + 1 > maxHomeLimit) && !player.isOp()) {
            player.sendMessage(generalConfiguration.getMaxHomeLimited());
            return false;
        }
        for (HomeEntry homeEntry : HomeManager.getPlayerHomeList(player)) {
            if (homeEntry.getName().equals(name)) {
                homeEntry.setX(location.getX());
                homeEntry.setY(location.getY());
                homeEntry.setZ(location.getZ());
                homeEntry.setYaw(location.getYaw());
                homeEntry.setPitch(location.getPitch());
                homeEntry.update();
                player.sendMessage(generalConfiguration.getHomeLocationUpdated());
                return true;
            }
        }
        new HomeEntry(name, player).save();
        player.sendMessage(generalConfiguration.getHomeSaved());
        return true;
    }
}
