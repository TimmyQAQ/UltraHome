package com.github.timmyovo.ultrahome.database;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "home_request_queue")
public class HomeRequestEntry extends HomeModel {
    @Id
    private UUID player;
    private UUID homeUid;
}
